<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/home/habitph/habit.ph/wp-content/plugins/wp-super-cache/' );
define('DB_NAME', 'habit_wp');

/** MySQL database username */
define('DB_USER', 'habitph');

/** MySQL database password */
define('DB_PASSWORD', 'H@bitPH2018');

/** MySQL hostname */
define('DB_HOST', 'mysql.habit.ph');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's3 2]]c*|SDj7lA7F`!8ICBjpa<gAoyz&Ic1sTXmrXKHm}Z}#NP9[i1*B^>zPPh6');
define('SECURE_AUTH_KEY',  'wL0_9a.4_Z8r*B/2k%6,E3oy5?7p{$.@boCxg*Q|#,8hdj4c?IP,#lmF^Rt%P!Wd');
define('LOGGED_IN_KEY',    'wE ?e(qU-vGSV[^7vJYlg.f 2&PSQ?)U^KS=XOzx?qb*}bS=e} GrP1R8%ZT.Nt/');
define('NONCE_KEY',        '~]hPp7{n< sAT#d-e7Sg,s@P`&/33SCTT-h5si+ohp(7w5naW~A&sCpc:H>L{.Bf');
define('AUTH_SALT',        'TU|U><twp&S#zZ$pl)D}bW +xDHjaTmJ1hkgwyq>G]SokE?>Ff)#/*Dbof=}-x1:');
define('SECURE_AUTH_SALT', ']8$%[!gH02~nmrme.A]Jk/8E`f>sZGhMxJX42#R-p;M4OqOq$N5fp1]. ^ov5q=o');
define('LOGGED_IN_SALT',   'tz%l<51K>L-;?=De&$*53Cescpy$os|_)nV5&#WVm$)-ydcZX3yQW2 6@_8fB[-B');
define('NONCE_SALT',       'xY6Jk^O~Br>Q2c)<nI3w Pn`%dDRdS$AGY`+RoX?y(X=TWpG02(j>]*=~|lVgoV1');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wphabit_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('FORCE_SSL_ADMIN', true);
// in some setups HTTP_X_FORWARDED_PROTO might contain 
// a comma-separated list e.g. http,https
// so check for https existence
if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
       $_SERVER['HTTPS']='on';
define('FORCE_SSL_LOGIN', true);
define ('WP_MEMORY_LIMIT', '300M');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
