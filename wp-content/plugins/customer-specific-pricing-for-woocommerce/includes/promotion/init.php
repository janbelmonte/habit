<?php

namespace wdmACMPromotion;

/**
 * Other Extensions
 *
 * To register the Quick Course Module.
 *
 * @package Advanced_Modules
 *
 * @since   1.0
 *
 *
 */
if (!class_exists('Promotion')) {

    class Promotion
    {
        private static $instance = null;
        private $pluginSlug = 'csp';

        /**
         * Private constructor to make this a singleton
         *
         * @access private
         */
        public function __construct()
        {
            // add_filter('__advc__register-modules', array($this, 'registerPromotion'), 999);
            add_action('csp_single_view_other_extensions', array($this, 'promotionPage'), 10);

            add_action('admin_enqueue_scripts', array($this, 'loadStyles'));
        }
        /**
         * Function to instantiate our class and make it a singleton
         */
        public static function getInstance()
        {
            if (!self::$instance) {
                self::$instance = new self;
            }

            return self::$instance;
        }
        /**
         * Function to register the Quick Course module
         *
         * @return     array  Quick Course Module settings
         */
        public function registerPromotion($settings)
        {
            $settings[] = array(
                'title'         => __("Other Extensions", ADVC_TEXT_DOMAIN), // Module's Box title
                'slug'          => "acm-promotion", // module slug, should be unique
                'box'           => false, // True, if want to show box on a main page
                'box_desc'      => "",
                // Description to show in a box
                's_page'        => true, // True, if module wants setting page
                's_page_title'  => __("Check Out Our Other Extensions", ADVC_TEXT_DOMAIN),
                // Setting page title, default value will be same as title.
                'full_desc'     => ""
            );
            return $settings;
        }

        public function promotionPage()
        {
            if (false === ($extensions = get_transient('_wdmcsp_extensions_data'))) {
                $extensions_json = wp_remote_get(
                    'https://wisdmlabs.com/products-thumbs/woo_extension.json',
                    array(
                        'user-agent' => 'Woocommerce Extensions Page'
                    )
                );

                if (!is_wp_error($extensions_json)) {
                    $extensions = json_decode(wp_remote_retrieve_body($extensions_json));

                    if ($extensions) {
                        set_transient('_wdmcsp_extensions_data', $extensions, 72 * HOUR_IN_SECONDS);
                    }
                }
            }
            include_once('partials/other-extensions.php');
            unset($extensions);
        }

        public function loadStyles()
        {
            global $wdmPluginDataCSP;
            if (!empty($_GET['tabie']) && 'other_extensions' == $_GET['tabie']) {
                $min = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG === true) ? '':'.min';

                wp_register_style($wdmPluginDataCSP['pluginSlug'] .'-promotion', plugins_url('assets/css/extension' . $min . '.css', __FILE__), array(), $wdmPluginDataCSP['pluginVersion']);

                // Enqueue admin styles
                wp_enqueue_style($wdmPluginDataCSP['pluginSlug'] .'-promotion');
            }
        }

        // End of functions
    }
    Promotion::getInstance();
}
