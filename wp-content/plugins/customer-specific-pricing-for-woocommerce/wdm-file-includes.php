<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

//including file for the working of Customer Specific Pricing on Simple Products
include_once(CSP_PLUGIN_URL.'/includes/user-specific-pricing/class-wdm-wusp-simple-products-usp.php');

//including file for the working of Customer Specific Pricing on Variable Products
include_once(CSP_PLUGIN_URL.'/includes/user-specific-pricing/class-wdm-wusp-variable-products-usp.php');

//including file for the working of Group Based Pricing on Simple Products
include_once(CSP_PLUGIN_URL.'/includes/group-specific-pricing/class-wdm-wusp-simple-products-gsp.php');

//including file for the working of Group Based Pricing on Variable Products
include_once(CSP_PLUGIN_URL.'/includes/group-specific-pricing/class-wdm-wusp-variable-products-gsp.php');

//including file for the working of Role Based Pricing on Simple Products
include_once(CSP_PLUGIN_URL.'/includes/role-specific-pricing/class-wdm-wusp-simple-products-rsp.php');

//including file for the working of Role Based Pricing on Variable Products
include_once(CSP_PLUGIN_URL.'/includes/role-specific-pricing/class-wdm-wusp-variable-products-rsp.php');

//including file for the working of Customer Specific Price on Products
include_once(CSP_PLUGIN_URL.'/includes/class-wdm-apply-usp-product-price.php');

//csp for order creation from backend
include_once(CSP_PLUGIN_URL.'/includes/dashboard-orders/class-wdm-customer-specific-pricing-new-order.php');

include_once(CSP_PLUGIN_URL.'/includes/class-wdm-single-view-tabs.php');

include_once(CSP_PLUGIN_URL.'/includes/class-wdm-wusp-ajax.php');

include_once(CSP_PLUGIN_URL.'/includes/class-wdm-wusp-functions.php');
